import {Component, Input} from '@angular/core';
import {Item} from "./app.component";

@Component({
  selector: 'app-tree-item',
  template: `
    <ng-container *ngIf="treeItem">
      {{treeItem.name}}
      <ng-container *ngIf="treeItem.type === 'tree' && treeItem.children.length">
        <ul>
          <li *ngFor="let child of treeItem.children">
            <app-tree-item [treeItem]="child"></app-tree-item>
          </li>
        </ul>
      </ng-container>
      <a *ngIf="treeItem.type === 'blob'" [href]="treeItem.link">link</a>
    </ng-container>
  `,
  styles: []
})
export class TreeItemComponent {
  @Input() treeItem: Item;
}
