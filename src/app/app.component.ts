import {Component} from '@angular/core';
import {Http, Response} from '@angular/http';
import 'rxjs/add/operator/toPromise';

export interface Item {
  name: string;
  path: string;
  type: string;
  children?: Item[];
  link: string;
}

interface Project {
  id: number;
  name: string;
  web_url: string;
}

const treeType: string = 'tree';
const fileType: string = 'blob';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
})
export class AppComponent {
  userName: string;
  projects: Project[];
  selectedProject: Project;
  gitlabItems: any[];
  tree: Item[];
  user: any;

  constructor(private http: Http) {
  }

  getUserProjects() {

    this.http.get(`https://gitlab.com/api/v4/users?username=${this.userName}`)
      .toPromise()
      .then((r: Response) => this.user = r.json()[0]);

    this.http.get(`https://gitlab.com/api/v4/users/${this.userName}/projects/`)
      .toPromise()
      .then((r) => r.json())
      .then((projects: Project[]) => this.projects = projects);
  }

  onSelectProject(project: Project) {

    const getPage: Function = (files: any[], pageIndex: number) => {
      return this.http.get(`https://gitlab.com/api/v4/projects/${project.id}/repository/tree?recursive=true&page=${pageIndex}&per_page=100`)
        .toPromise()
        .then((r: Response) => {
          files = files.concat(r.json());
          const nextPageIndex: number = parseInt(r.headers.get('X-Next-Page'));
          return !nextPageIndex ? Promise.resolve(files) : getPage(files, nextPageIndex);
        })
    };

    getPage([], 1)
      .then((files: Item[]) => {
        return this.gitlabItems = files.map((i: Item) => {
          if (i.type === treeType) {
            return i;
          }
          i.link = `https://gitlab.com/api/v4/projects/${project.id}/repository/files/${encodeURIComponent(i.path)}/raw?ref=master`;
          return i;
        })
      })
      .then(() => this.drawTree());
  }

  drawTree(): void {
    function getParentItems(pathItems: string[], tree: Item[]): Item[] {
      return pathItems.reduce((items: Item[], pointName: string) => {
        return pointName ? items.find((i: Item) => i.name === pointName).children : items;
      }, tree);
    }

    const sortedItems: any[] = [].concat(this.gitlabItems);
    sortedItems.sort((point1, point2) => {
      const path1Words = point1.path.split('/');
      const path2Words = point2.path.split('/');
      return path1Words.length > path2Words.length ? 1 : -1;
    });

    const tree: Item[] = [];
    sortedItems.forEach((i: Item) => {
      const pathItems: string[] = i.path.split('/');
      const newItem: Item = Object.assign({}, i, {name: pathItems.pop()});
      if (i.type === treeType) {
        newItem.children = [];
      }
      getParentItems(pathItems, tree).push(newItem);
    });

    this.tree = tree;
  }

}
